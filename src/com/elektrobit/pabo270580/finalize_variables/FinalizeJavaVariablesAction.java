package com.elektrobit.pabo270580.finalize_variables;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.arrangement.std.StdArrangementTokens;
import com.intellij.psi.impl.source.tree.java.PsiJavaTokenImpl;
import com.intellij.psi.search.LocalSearchScope;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.psi.util.PsiElementFilter;
import com.intellij.psi.util.PsiTreeUtil;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;


/**
 * This class handles the action to make all qualified variables of a Java file final.
 *
 * The logic to determine the eligibility for the final modifier of variables
 * is used to both perform the actual task to add the final modifier to said variables
 * as well as to enable/disable this action in the UI.
 */
public class FinalizeJavaVariablesAction extends AnAction implements AnAction.TransparentUpdate {

    /**
     * The final modifier.
     */
    private static final String FINAL =     StdArrangementTokens.Modifier.FINAL.toString();
    /**
     * The static modifier.
     */
    private static final String STATIC =    StdArrangementTokens.Modifier.STATIC.toString();
    /**
     * The volatile modifier.
     */
    private static final String VOLATILE =  StdArrangementTokens.Modifier.VOLATILE.toString();
    /**
     * The abstract modifier.
     */
    private static final String ABSTRACT =  StdArrangementTokens.Modifier.ABSTRACT.toString();

    /**
     * Constant that emulates the amount of assignments within a loop.
     * For the determination of the eligibility of the final modifier for a variable,
     * such a sample value > 1 is sufficient.
     */
    private static final int WITHIN_LOOP_ASSIGNMENT_COUNT = Short.MAX_VALUE;

    /**
     * Checks whether the {@link PsiFile} is a writable java file.
     *
     * @param psiFile the {@link PsiFile} to check.
     *
     * @return true in case the given {@link PsiFile} is a writable java file.
     */
    private boolean isFinalEligibleFile(final PsiFile psiFile) {
        return (psiFile instanceof PsiJavaFile)                     //java file is selected
                && (psiFile.isWritable())                           //java file is writable
                && ((PsiJavaFile) psiFile).getClasses().length > 0; //java class(es) contained in java file
    }

    @Override
    public void actionPerformed(final AnActionEvent event) {

        final PsiFile psiFile = event.getData(CommonDataKeys.PSI_FILE);
        //do nothing if no writable java file is selected
        if (!isFinalEligibleFile(psiFile)) {
            return;
        }

        //retrieve variables of that class that are qualified to be made final
        final PsiClass[] classes = ((PsiJavaFile) psiFile).getClasses();
        final List<PsiVariable> variablesToFinalize = getFinalEligibleVariables(classes[0]);

        if (!variablesToFinalize.isEmpty()) {
            final Project project = event.getData(PlatformDataKeys.PROJECT);
            //create and execute the action to make the variables final
            final WriteCommandAction.Simple writeCommandAction =
                    new WriteCommandAction.Simple(project, getTemplatePresentation().getText()) {
                        @Override
                        protected void run() {
                            for (final PsiVariable variable : variablesToFinalize) {
                                if (variable != null && variable.getModifierList() != null) {
                                    variable.getModifierList().setModifierProperty(FINAL, true);
                                }
                            }
                        }
                    };
            writeCommandAction.execute();
        }

        //retrieve variables of that class that are falsely final
        final List<PsiVariable> variablesToUnfinalize = getFalselyFinalVariables(classes[0]);

        if (!variablesToUnfinalize.isEmpty()) {
            final Project project = event.getData(PlatformDataKeys.PROJECT);
            //create and execute the action to make the falsely final variables non-final
            final WriteCommandAction.Simple writeCommandAction =
                    new WriteCommandAction.Simple(project, getTemplatePresentation().getText()) {
                        @Override
                        protected void run() {
                            for (final PsiVariable variable : variablesToUnfinalize) {
                                if (variable != null && variable.getModifierList() != null) {
                                    variable.getModifierList().setModifierProperty(FINAL, false);
                                }
                            }
                        }
                    };
            writeCommandAction.execute();
        }
    }

    @Override
    public void update(final AnActionEvent event) {
        super.update(event);

        final PsiFile psiFile = event.getData(CommonDataKeys.PSI_FILE);
        //disable action if no writable java file is selected
        if (!isFinalEligibleFile(psiFile)) {
            event.getPresentation().setEnabled(false);
            return;
        }

        final Project project = event.getData(PlatformDataKeys.PROJECT);
        if (project == null || DumbService.isDumb(project)) {
            // if project is in dumb mode (e.g. indexing)
            event.getPresentation().setEnabled(false);
        } else {
            // if project is not in dumb mode
            // enable action if there is a variable qualified to be made final or a variable that is falsely final
            final PsiClass[] classes = ((PsiJavaFile) psiFile).getClasses();
            final List<PsiVariable> nonFinalizedVariables = getFinalEligibleVariables(classes[0]);
            final List<PsiVariable> falselyFinalVariables = getFalselyFinalVariables(classes[0]);
            event.getPresentation().setEnabled(!nonFinalizedVariables.isEmpty() || !falselyFinalVariables.isEmpty());
        }
    }

    /**
     * Retrieves all variables that are eligible to be made final.
     *
     * @param clazz the class which will be scanned for potential variables to be made final
     * @return the list of variables that are qualified to be made final
     */
    private List<PsiVariable> getFinalEligibleVariables(final PsiClass clazz) {

        final PsiElement[] elementsToFinalize = PsiTreeUtil.collectElements(clazz, new PsiElementFilter() {
            public boolean isAccepted(final PsiElement element) {

                //is variable
                if (element instanceof PsiVariable) {
                    final PsiVariable variable = (PsiVariable) element;

                    // if variable is parameter
                    if (variable instanceof PsiParameter) {
                        // get 2nd parent since 1st parent is PsiParameterList
                        final PsiElement parent = variable.getParent() == null ? null : variable.getParent().getParent();

                        if (parent instanceof PsiLambdaExpression) {
                            // if variable is lambda expression parameter - parameter is not final eligible
                            return false;
                        } else if (parent instanceof PsiMethod) {
                            // if method is abstract - parameter is not final eligible
                            final PsiMethod method = (PsiMethod) parent;
                            if (method.getModifierList().hasModifierProperty(ABSTRACT)) {
                                return false;
                            }
                        }
                    }

                    final PsiModifierList modifierList = variable.getModifierList();
                    if (modifierList == null || !modifierList.hasModifierProperty(FINAL)) {
                        return isFinalEligible(variable);
                    }
                }
                return false;
            }
        });

        final PsiVariable[] variablesToFinalize = new PsiVariable[elementsToFinalize.length];
        for (int i = 0; i< elementsToFinalize.length; ++i) {
            variablesToFinalize[i] = (PsiVariable) elementsToFinalize[i];
        }
        return new LinkedList<>(Arrays.asList(variablesToFinalize));
    }

    /**
     * Retrieves all variables that have been falsely made final.
     *
     * @param clazz the class which will be scanned for wrongfully made final variables
     * @return the list of variables that have been falsely made final
     */
    private List<PsiVariable> getFalselyFinalVariables(final PsiClass clazz) {

        final PsiElement[] falselyFinalElements = PsiTreeUtil.collectElements(clazz, new PsiElementFilter() {
            public boolean isAccepted(final PsiElement element) {

                // final all parameters
                if (element instanceof PsiParameter) {
                    final PsiParameter parameter = (PsiParameter) element;

                    // get method from parameter and check if method is abstract
                    if (parameter.getParent().getParent() instanceof PsiMethod) {
                        // if method is abstract...
                        final PsiMethod method = (PsiMethod) parameter.getParent().getParent();
                        final boolean methodIsAbstract = method.getModifierList().hasModifierProperty(ABSTRACT);
                        // ... and parameter is final
                        final PsiModifierList modifList = parameter.getModifierList();
                        final boolean parameterIsFinal = modifList != null && modifList.hasModifierProperty(FINAL);
                        return methodIsAbstract && parameterIsFinal;
                    }
                }
                return false;
            }
        });

        final PsiVariable[] falselyFinalVariables = new PsiVariable[falselyFinalElements.length];
        for (int i = 0; i< falselyFinalElements.length; ++i) {
            falselyFinalVariables[i] = (PsiVariable) falselyFinalElements[i];
        }
        return new LinkedList<>(Arrays.asList(falselyFinalVariables));
    }

    /**
     * Determines if a variable is eligible to be made final.
     * @param variable the variable under consideration of eligibility for final
     * @return true if the variable could be made final, otherwise false
     */
    private boolean isFinalEligible(final PsiVariable variable) {

        if (variable instanceof PsiParameter) {
            //variable is a parameter:
            return isFinalEligibleParameter((PsiParameter) variable);

        } else if (variable instanceof PsiField) {
            //variable is a member field:
            return isFinalEligibleField((PsiField) variable);

        } else {
            //variable is local variable:
            return isFinalEligibleLocalVariable(variable);

        }
    }

    /**
     * Determines whether a parameter is qualified to be made final.
     *
     * @param parameter the parameter under consideration of eligibility for final
     * @return true if the parameter could be made final, otherwise false
     */
    private boolean isFinalEligibleParameter(final PsiParameter parameter) {
        final int assignmentCount = getAssignmentExpressionCount(parameter, null);
        return (assignmentCount == 0);
    }

    /**
     * Determines whether a member variable is qualified to be made final.
     *
     * @param field the member variable under consideration of eligibility for final
     * @return true if the member variable could be made final, otherwise false
     */
    private boolean isFinalEligibleField(final PsiField field) {

        final boolean declarationHasAssignment = hasDeclarationAssignment(field);
        final int totalAssignmentCount = getAssignmentExpressionCount(field, null);

        if (field.getModifierList() == null || field.getModifierList().hasModifierProperty(VOLATILE)) {
            return false;
        }
        else if (field.getModifierList().hasModifierProperty(STATIC)) {
            //if field is static -> must be initialized and must not have any further assignments
            return declarationHasAssignment && totalAssignmentCount == 0;
        } else {
            if (declarationHasAssignment) {
                //if field has been assigned a value on declaration:

                //no further value assignment should have been made
                return (totalAssignmentCount == 0);
            } else {
                //if field has not been assigned a value on declaration:

                //retrieve constructors of field's class
                final PsiMethod[] constructors = getConstructorsByField(field);
                if (constructors == null || constructors.length == 0) {
                    return false;
                }
                //number of total assignments must match number of constructors
                if (totalAssignmentCount != constructors.length) {
                    return false;
                }

                //each constructor must assign a value to the variable exactly once
                int constructorAssignmentCont;
                for (final PsiMethod constructor : constructors) {
                    constructorAssignmentCont = getAssignmentExpressionCount(field,
                            new LocalSearchScope(constructor));
                    //if any constructor has not exactly 1 assignment declaration of this variable
                    if (constructorAssignmentCont != 1) {
                        return false;
                    }
                }
                //all constructors had exactly one assignment of the field
                return true;
            }
        }
    }

    /**
     * Determines whether a local variable is qualified to be made final.
     *
     * @param variable the local variable under consideration of eligibility for final
     * @return true if the local variable could be made final, otherwise false
     */
    private boolean isFinalEligibleLocalVariable(final PsiVariable variable) {
        int assignmentCount = getAssignmentExpressionCount(variable, null);
        final boolean declarationHasAssignment = hasDeclarationAssignment(variable);
        if (declarationHasAssignment) {
            ++assignmentCount;
        }
        return (assignmentCount <= 1);
    }

    /**
     * Determines whether the passed variable has been assigned a value
     * with its declaration.
     *
     * @param variable the variable which will be examined for
     *                 data assignment on declaration
     * @return true if the variable has been assigned a value during its declaration,
     *         otherwise false
     */
    private boolean hasDeclarationAssignment(final PsiVariable variable) {

        final PsiElement[] declarationComponents = variable.getChildren();

        for (final PsiElement declarationComponent : declarationComponents) {
            if (declarationComponent instanceof PsiJavaTokenImpl) {
                final PsiJavaTokenImpl javaToken = (PsiJavaTokenImpl) declarationComponent;
                if (javaToken.getTokenType().equals(JavaTokenType.EQ)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Retrieves the number of times a variable has been assigned a value.
     *
     * @param variable the variable under examination
     * @param localSearchScope the search scope to narrow down the assignment expression count search
     * @return the amount of times the variable has been assigned a value.
     *         Since assignments within a loop have a conditional amount of assignments,
     *         the actual value is undefined. Hence a fixed value will be returned (Short.MAX_VALUE)
     *         which is sufficient for the final modifier eligibility determination of a variable.
     */
    private int getAssignmentExpressionCount(final PsiVariable variable,
                                             final LocalSearchScope localSearchScope) {

        final Collection<PsiReference> variableReferences;
        if (localSearchScope == null) {
            variableReferences = ReferencesSearch.search(variable).findAll();
        } else {
            variableReferences = ReferencesSearch.search(variable, localSearchScope).findAll();
        }

        int count = 0;
        PsiElement variableUsage;
        for (final PsiReference variableReference : variableReferences) {
            variableUsage = variableReference.getElement().getParent();

            if (variableUsage instanceof PsiAssignmentExpression) {
                //if  variable is assignee
                if (variableReference.equals(((PsiAssignmentExpression) variableUsage).getLExpression())) {
                    if (isInsideLoop(variableReference)) {
                        return WITHIN_LOOP_ASSIGNMENT_COUNT;
                    }
                    ++count;
                }
            } else if (variableUsage instanceof PsiPrefixExpression
                    || variableUsage instanceof PsiPostfixExpression) {
                if (isInsideLoop(variableReference)) {
                    return WITHIN_LOOP_ASSIGNMENT_COUNT;
                }
                ++count;
            }
        }
        return count;
    }

    /**
     * Determines whether a variable was referred to within a loop.
     *
     * @param variableReference the variable to be investigated
     * @return true if the variable was referred to from within a loop, otherwise false
     */
    private boolean isInsideLoop(final PsiReference variableReference) {

        for (PsiElement element = variableReference.getElement().getParent();
             element != null; element = element.getParent()) {
            if (element instanceof PsiMethod || element instanceof PsiClass) {
                return false;
            } else if (element instanceof PsiLoopStatement) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieves the constructors of the class of a member variable.
     *
     * @param field the member variable
     *
     * @return an array of constructors of the class of the member variable
     *         or null if none could be found.
     */
    private PsiMethod[] getConstructorsByField(final PsiField field) {
        for (PsiElement element = field.getParent(); element != null; element = element.getParent()) {
            if (element instanceof PsiClass) {
                final PsiClass clazz = (PsiClass) element;
                return clazz.getConstructors();
            }
        }
        return null;
    }

}
