# README #

This is an IntelliJ IDEA Plugin that is aimed at making all eligible variables of a Java Class final.
Currently there are still issues with variable assignement checks regarding branch coverage, hence not all eligible variables will be made final.
Related to this issue there are also cases in which variables will be made final even though they should not be final.
As a result of these issues, in its current state this plugin should be used as a helping tool in addition to the developer's common sense.

Version 0.1.4